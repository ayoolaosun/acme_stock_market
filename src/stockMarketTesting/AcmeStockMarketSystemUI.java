package stockMarketTesting;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class AcmeStockMarketSystemUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AcmeStockMarketSystemUI frame = new AcmeStockMarketSystemUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AcmeStockMarketSystemUI() {
		setTitle("Operation Selection");
		setForeground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 507, 389);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAcmeStockMarket = new JLabel("Acme Stock Market");
		lblAcmeStockMarket.setFont(new Font("Consolas", Font.BOLD, 24));
		lblAcmeStockMarket.setBounds(118, 11, 246, 48);
		contentPane.add(lblAcmeStockMarket);
		
		JButton btnBuyStock = new JButton("BUY STOCK");
		btnBuyStock.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnBuyStock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				BuyStockUI buyStockUI = new BuyStockUI();
				buyStockUI.setVisible(true);
				
			}
		});
		btnBuyStock.setBounds(31, 83, 118, 23);
		contentPane.add(btnBuyStock);
		
		JButton btnSellStock = new JButton("SELL STOCK");
		btnSellStock.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnSellStock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				SellStockUI sellStockUI = new SellStockUI();
				sellStockUI.setVisible(true);
				
			}
		});
		btnSellStock.setBounds(320, 83, 106, 23);
		contentPane.add(btnSellStock);
		
		JButton btnShowProfit = new JButton("SHOW PROFIT");
		btnShowProfit.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnShowProfit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				ShowProfitUI showProfitUI = new ShowProfitUI();
				showProfitUI.setVisible(true);
			}
		});
		btnShowProfit.setBounds(31, 195, 118, 23);
		contentPane.add(btnShowProfit);
		
		JButton btnViewLast = new JButton("VIEW LAST 5 STOCK CHANGES");
		btnViewLast.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnViewLast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				FiveStockChangesUI fiveStockChangesUI  = new FiveStockChangesUI();
				fiveStockChangesUI.setVisible(true);
			}
		});
		btnViewLast.setBounds(204, 138, 222, 23);
		contentPane.add(btnViewLast);
		
		JButton btnViewStocks = new JButton("VIEW STOCKS");
		btnViewStocks.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnViewStocks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				ViewStockUI viewStockUI  = new ViewStockUI();
				viewStockUI.setVisible(true);
			}
		});
		btnViewStocks.setBounds(31, 138, 118, 23);
		contentPane.add(btnViewStocks);
		
		JButton btnShowStockTrnd = new JButton("SHOW STOCK TREND AND PRICE");
		btnShowStockTrnd.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnShowStockTrnd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				TrendsAndPricesUI trendsAndPricesUI = new TrendsAndPricesUI();
				trendsAndPricesUI.setVisible(true);
				
				
			}
		});
		btnShowStockTrnd.setBounds(204, 195, 222, 23);
		contentPane.add(btnShowStockTrnd);
		
		JButton btnAutomaticallyBuyAnd = new JButton("AUTOMATICALLY BUY AND SELL SHARES");
		btnAutomaticallyBuyAnd.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnAutomaticallyBuyAnd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				AutomationUI automationUI  = new AutomationUI();
				automationUI.setVisible(true);
				
			}
		});
		btnAutomaticallyBuyAnd.setBounds(31, 246, 395, 23);
		contentPane.add(btnAutomaticallyBuyAnd);
		
		JButton btnStop = new JButton("LOGOUT");
		btnStop.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IServerConnection connect = ServerConnectionSingleton.getInstance();
				connect.closeConnection();
				setVisible(false);
				dispose();
			}
		});
		btnStop.setBounds(173, 316, 106, 23);
		contentPane.add(btnStop);
	}
}
