/**
 * 
 */
package stockMarketTesting;

import java.util.ArrayList;

/**
 * @author Ayo
 * Manages buying Stocks from Stock Market
 * Implements: IServerTransaction
 * @param Stock
 */
public class BuyStockFromServer implements IServerTransaction {
	IServerConnection connect = ServerConnectionSingleton.getInstance();
	private Stock buyStock; 
	private String id;
	private String transaction; 
	private CommunicationProtocol communicationProtocol;
	private String communication;
    ArrayList<String> returnedMessage = new ArrayList<String>();

	/**
	 * Assign the stock to be Bought
	 * @param buyStock
	 */
	public BuyStockFromServer(Stock buyStock, String id)
	{
		this.buyStock = buyStock;
		this.setId(id);
		this.setTransaction("BUY");
	}
	/**
	 * Executes the selling of Stock to Server
	 */
	public ArrayList<String> run()
	{
		this.communicationProtocol = new CommunicationProtocol(this.getTransaction(),this.buyStock.getName(),this.buyStock.getBuyQuantity(),this.getId());
		this.communicationProtocol.getCommunicationProtocol();
		this.communication = this.communicationProtocol.getCommunicationProtocol();
		returnedMessage = connect.readMessage(this.communication);
		return returnedMessage;
		//
	}
	
	
	/**
	 * @return the transaction
	 */
	public String getTransaction() {
		return transaction;
	}
	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	public String test(){
		return this.communication;
	}
	
}
