package stockMarketTesting;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Color;

public class BuyStockUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtStockName;
	private JTextField txtStockQuantity;
	private ServerCommunicationFacade communicationFacade = new ServerCommunicationFacade();
	private IStockAccount stockAccount = StockAccount.getStockAccount();
    ArrayList<String> returnedMessage = new ArrayList<String>();
    private Collection<Stock>  buyStocks = new ArrayList<Stock>();
	Hashtable<String, Stock> stocks = new Hashtable<String, Stock>(); 
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuyStockUI frame = new BuyStockUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuyStockUI() {
		setTitle("Buy Stock");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1025, 392);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnBack = new JButton("BACK");
		btnBack.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AcmeStockMarketSystemUI frame1 = new AcmeStockMarketSystemUI();
				frame1.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
		btnBack.setBounds(10, 30, 89, 23);
		contentPane.add(btnBack);
		
		JLabel lblTitle = new JLabel("Acme Stock Market");
		lblTitle.setFont(new Font("Consolas", Font.BOLD, 24));
		lblTitle.setBounds(164, 11, 246, 48);
		contentPane.add(lblTitle);
		
		JLabel lblStockName = new JLabel("Stock Name");
		lblStockName.setFont(new Font("Consolas", Font.PLAIN, 11));
		lblStockName.setBounds(10, 92, 75, 14);
		contentPane.add(lblStockName);
		
		JLabel lblStockQuantity = new JLabel("Stock Quantity");
		lblStockQuantity.setFont(new Font("Consolas", Font.PLAIN, 11));
		lblStockQuantity.setBounds(10, 147, 89, 14);
		contentPane.add(lblStockQuantity);
		
		txtStockName = new JTextField();
		txtStockName.setFont(new Font("Consolas", Font.PLAIN, 11));
		txtStockName.setBounds(160, 89, 172, 20);
		contentPane.add(txtStockName);
		txtStockName.setColumns(10);
		
		txtStockQuantity = new JTextField();
		txtStockQuantity.setFont(new Font("Consolas", Font.PLAIN, 11));
		txtStockQuantity.setBounds(160, 144, 45, 20);
		contentPane.add(txtStockQuantity);
		txtStockQuantity.setColumns(10);
		
		JLabel txtCurrentStockList = new JLabel("a");
		txtCurrentStockList.setFont(new Font("Consolas", Font.PLAIN, 11));
		txtCurrentStockList.setVerticalAlignment(SwingConstants.TOP);
		txtCurrentStockList.setBounds(745, 80, 184, 262);
		contentPane.add(txtCurrentStockList);
		
		final JLabel txtPurchaseStockList = new JLabel("");
		txtPurchaseStockList.setFont(new Font("Consolas", Font.PLAIN, 11));
		txtPurchaseStockList.setVerticalAlignment(SwingConstants.TOP);
		txtPurchaseStockList.setBounds(472, 80, 150, 262);
		contentPane.add(txtPurchaseStockList);
		
		JButton btnBuyStocks = new JButton("PLACE BUY STOCK ORDER");
		btnBuyStocks.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnBuyStocks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!buyStocks.isEmpty()){
					
					returnedMessage = communicationFacade.buyStock(stockAccount.getBuyStocks());
					try {		 
					    TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
					    //Handle exception
					}
					stocks = stockAccount.getStocks();
					String appendString ="<html><U>Stock Name   Quantity</U>";
					for(String aMessage: returnedMessage){
						appendString = appendString + "<p>" + aMessage+ "</p>";
						
					}
					txtCurrentStockList.setText(appendString + "</html>");
					
				}
			}
		});
		btnBuyStocks.setBounds(160, 221, 203, 23);
		contentPane.add(btnBuyStocks);
		
		JButton btnCancel = new JButton("CANCEL");
		btnCancel.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnCancel.setBounds(373, 221, 89, 23);
		contentPane.add(btnCancel);
		
		
		
		JButton btnAddToBuy = new JButton("ADD TO BUY STOCK ORDER");
		btnAddToBuy.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnAddToBuy.addActionListener(new ActionListener() {
			/**
			 * Add stock to buy Order List
			 */
			public void actionPerformed(ActionEvent e) {
				String stockName;
				int quantity;
				stockName = txtStockName.getText();
				quantity= Integer.parseInt(txtStockQuantity.getText());
				//Create new stock 
				//Add Stock to Potential Stock List of Stock Account
				communicationFacade.createStock(stockName, quantity);
			 
				
				buyStocks = stockAccount.getBuyStocks();
				String appendTest ="<html><U>Stock Name   Quantity</U>";
				for (Stock stock: buyStocks) {
					appendTest = appendTest +"<p>"+ stock.getName()+ " " + Integer.toString(stock.getBuyQuantity())+ "</p> ";
			     }
				txtPurchaseStockList.setText(appendTest +"</html>");


			}
		});
		btnAddToBuy.setBounds(215, 143, 247, 23);
		contentPane.add(btnAddToBuy);
		
		
		
		JLabel lblPurchaseStockList = new JLabel("Purchase Stock List");
		lblPurchaseStockList.setFont(new Font("Consolas", Font.PLAIN, 11));
		lblPurchaseStockList.setBounds(475, 55, 128, 14);
		contentPane.add(lblPurchaseStockList);
		
		JLabel lblCurrentstocklist = new JLabel("CurrentStockList");
		lblCurrentstocklist.setFont(new Font("Consolas", Font.PLAIN, 11));
		lblCurrentstocklist.setBounds(745, 55, 128, 14);
		contentPane.add(lblCurrentstocklist);
		//Display Current Stock List
		Collection<Stock> stocks = stockAccount.getStocks().values();
		String test2 ="<html><U>Stock Name   Quantity</U>";
		
		for (Stock stock: stocks) {
	    	  test2 = test2 + "<p>" + stock.getName()+ "= " + Integer.toString(stock.getQuantity())+ "</p> ";
	     }
		txtCurrentStockList.setText(test2 + "</html>");
	}
}
