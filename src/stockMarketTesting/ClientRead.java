package stockMarketTesting;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

public class ClientRead implements Runnable {
	Socket clientSocket = null;
    BufferedReader in = null;
    String inString;
    ArrayList<String> returnedMessage = new ArrayList<String>();
    Boolean running = true;

    public ClientRead(Socket aSocket)
    {
        clientSocket = aSocket;
        try
        {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        }
        catch(IOException e)
        {
            System.out.println("Error: " + e);
        }
    }

    public void readFromServer()
    {
        try
        {
            while(!(inString = in.readLine()).equals(""))
            {
            	System.out.println(inString);
            	returnedMessage.add(inString);
            	ManageServerStockTransaction stockTransaction = new	ManageServerStockTransaction(inString);
    	    	
            }
//            running = false;
        	
        }
        catch(IOException e)
        {
            System.out.println("Error: " + e);
        }
    }

    
    public ArrayList<String> getReturnedMessage(){
    	
    	return this.returnedMessage;
    }  
    
    public void run()
    {
    	while(true){
    		readFromServer();
    	}
    }
    
 

}
