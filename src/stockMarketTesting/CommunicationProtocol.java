/**
 * 
 */
package stockMarketTesting;

/**
 * @author Ayo
 *
 */
public class CommunicationProtocol {
	private String delimiter; 
	private String transaction;
	private String name;
	private int quantity;
	private String id; 
	private String communicationProtocol;  
	public CommunicationProtocol(String transaction,String name, int quantity, String id){
		this.setTransaction(transaction);
		this.setId(id);
		this.setName(name);
		this.setQuantity(quantity);
		this.setDelimiter(":");
		System.out.println(this.getCommunicationProtocol());
	}
	/**
	 * @return the transaction
	 */
	public String getTransaction() {
		return transaction;
	}
	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the delimiter
	 */
	public String getDelimiter() {
		return delimiter;
	}
	/**
	 * @param delimiter the delimiter to set
	 */
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
	/**
	 * @return the communicationProtocol
	 */
	public String getCommunicationProtocol() {
		//TRA:NAME:QUN:ID
		String newTransaction, newName, newQuantity, newId, newCommunicationProtocol;
		newTransaction = this.getTransaction();
		newName = this.getName();
		newQuantity = Integer.toString(this.getQuantity());
		newId = this.getId();
		newCommunicationProtocol = newTransaction+this.getDelimiter()+newName+this.getDelimiter()+newQuantity+this.getDelimiter()+newId;
		this.setCommunicationProtocol(newCommunicationProtocol);
		return this.communicationProtocol;
	}
	/**
	 * @param communicationProtocol the communicationProtocol to set
	 */
	public void setCommunicationProtocol(String communicationProtocol) {
		this.communicationProtocol = communicationProtocol;
	}
}
