package stockMarketTesting;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FiveStockChangesUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FiveStockChangesUI frame = new FiveStockChangesUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FiveStockChangesUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("BACK");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AcmeStockMarketSystemUI frame1 = new AcmeStockMarketSystemUI();
				frame1.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
		button.setBounds(10, 30, 89, 23);
		contentPane.add(button);
		
		JLabel label = new JLabel("Acme Stock Market");
		label.setFont(new Font("Segoe UI", Font.BOLD, 24));
		label.setBounds(139, 5, 246, 48);
		contentPane.add(label);
	}

}
