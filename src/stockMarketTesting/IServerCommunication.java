/**
 * 
 */
package stockMarketTesting;

/**
 * @author Ayo
 * Contract to communicate with the Stock Market Server
 * An initial Message, which assigns ID is acquired
 */
public interface IServerCommunication {
	public void initialMessage();
}
