package stockMarketTesting;

import java.util.ArrayList;

/**
 * 
 * @author Ayo
 * Contract to register to Server
 * Sets ID used for communication 
 */

public interface IServerConnection {
	public void connectToServer();
	public String getId();
	public void sendMessage(String message);
	public ArrayList<String> readMessage(String message);
	public String getServerMessage();
	public void closeConnection();
}
