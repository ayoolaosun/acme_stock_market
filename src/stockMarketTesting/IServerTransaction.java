/**
 * 
 */
package stockMarketTesting;

import java.util.ArrayList;

/**
 * @author Ayo
 * Contract for Stock Transaction with Server
 * Implementers: BuyStockFromServer, SellStockToServer
 */
public interface IServerTransaction {
	public ArrayList<String> run();
}
