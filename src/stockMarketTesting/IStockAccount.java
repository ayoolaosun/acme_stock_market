package stockMarketTesting;

import java.util.Collection;
import java.util.Hashtable;

public interface IStockAccount {
	public Hashtable<String, Stock> getStocks();
	public void addToStock(Stock stock);
	public void removeFromStock(Stock stock);
	public Collection<Stock> getSellStocks(); 
	public void setSellStocks(Collection<Stock> sellStocks); 
	public void addToSellStocks(Stock newStock);
	public void removeFromSellStock(Stock newStock);
	public Collection<Stock> getBuyStocks(); 
	public void setBuyStocks(Collection<Stock> buyStocks);	
	public void addToBuyStock(Stock newStock);
	public void removeFromBuyStock(Stock newStock);
	public void subtractFromBalance(double subtractAmount);
	public void addToBalance(double addedAmount);
	public double getCurrentBalance();
}
