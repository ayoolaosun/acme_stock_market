package stockMarketTesting;

public class ManageServerStockTransaction {
	IStockAccount stockAccount = StockAccount.getStockAccount();
	
	public ManageServerStockTransaction (String serverMessage){
		decideTransactionType(serverMessage);
		
	}
	
	
	public void manageBuyStockFromServer(String stockName, String stockPrice){
		//Get the Stock using stock name returned by the server
		//Increment Quantity
		Stock stock = stockAccount.getStocks().get(stockName);
		 stockAccount.getStocks().get(stock.getName())
		 .setQuantity(stock.getBuyQuantity()+
		 stockAccount.getStocks().get(stock.getName()).getQuantity());
		 //Set Buy Quantity as zero
		 stockAccount.getStocks().get(stock.getName())
		 .setBuyQuantity(0);
		 //Decrements Current Balance using Stock Price
		 stockAccount.subtractFromBalance(Double.parseDouble(stockPrice));
	} 
	
	public void manageSellStockToServer(String stockName, String stockPrice){
		 Stock stock = stockAccount.getStocks().get(stockName);
		 stockAccount.getStocks().get(stock.getName())
		 .setQuantity(stock.getQuantity()-
		 stockAccount.getStocks().get(stock.getName()).getSellQuantity());
		 //Set Sell Quantity as zero
		 stockAccount.getStocks().get(stock.getName())
		 .setSellQuantity(0);
		 //Increment Current Balance to Check Profit Made
		 stockAccount.addToBalance(Double.parseDouble(stockPrice));
	}
	
	public void decideTransactionType(String message){
		
		final String[] allServerMessage= message.split(":");
		//String firstValue = allServerMessage[allServerMessage.length-1];
		int operationLenght = allServerMessage.length-1;
		//Checks for which message as been send back from server
		String operationDecider="";
		//Check for Buy or Sell
		if ( operationLenght== 3){
			operationDecider= allServerMessage[0];
			//Check for Stock Trend or Time
			if(operationDecider.equals("STK")){
				System.out.println("STK");
			}else if(operationDecider.equals("TIME")){
				System.out.println("TIME");
			}else if(operationDecider.equals("END")){
				System.out.println("END");
			}
		}else if(operationLenght== 5){
			//Check for Buy or Sell
			operationDecider= allServerMessage[1];
			if(operationDecider.equals("BUY")){
					//Ensures Stock exists in the Stock HashTable 
					if(stockAccount.getStocks().containsKey(allServerMessage[3])){
						//Get the Stock using stock name returned by the server
						//Increment Quantity
						manageBuyStockFromServer(allServerMessage[3], allServerMessage[5]);
					}
			}else if(operationDecider.equals("SELL")){
				//Ensures Stock exists in the Stock HashTable 
				if(stockAccount.getStocks().containsKey(allServerMessage[3])){
					//Get the Stock using stock name returned by the server
					//Increment Quantity
					manageSellStockToServer(allServerMessage[3], allServerMessage[5]);
				}
			}
		}
	
	}
}
