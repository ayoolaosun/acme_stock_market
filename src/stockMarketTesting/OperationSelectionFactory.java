package stockMarketTesting;

/**
 * @author Ayo
 * Factory for sending and receiving message from Stock Market 
 *
 */
public class OperationSelectionFactory {
	//IServerCommunication communicationType;
		
	/**
	 * Decides what operation is performed Send or Receive Message 
	 * @param selection
	 * @return IServerCommunication
	 */
		public IServerCommunication selectOperation(String selection)
		{
			if(selection == null){
				return null;
			}	
			
			if (selection.equalsIgnoreCase("sendMessage"))
			{				
				return new SendMessage();				
			}
			else if (selection.equalsIgnoreCase("receiveMessage"))
			{
				return new ReceiveMessage();
			}
			return null;
		}
}
