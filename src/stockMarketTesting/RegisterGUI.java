package stockMarketTesting;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class RegisterGUI {

	private JFrame frmAchmeRegister;
	private JTextField txtName;
	private JTextField txtPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public 	void run() {
				try {
					RegisterGUI window = new RegisterGUI();
					window.frmAchmeRegister.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RegisterGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAchmeRegister = new JFrame();
		frmAchmeRegister.setTitle("Achme Register");
		frmAchmeRegister.getContentPane().setFont(new Font("Consolas", Font.PLAIN, 11));
		frmAchmeRegister.getContentPane().setBackground(Color.WHITE);
		frmAchmeRegister.setForeground(Color.WHITE);
		frmAchmeRegister.setFont(new Font("Calibri", Font.PLAIN, 12));
		frmAchmeRegister.getContentPane().setForeground(Color.GRAY);
		frmAchmeRegister.setBounds(100, 100, 450, 300);
		frmAchmeRegister.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAchmeRegister.getContentPane().setLayout(null);
		
		JLabel lblAtm = new JLabel("Acme Stock Market");
		lblAtm.setFont(new Font("Consolas", Font.BOLD, 24));
		lblAtm.setBounds(96, 0, 246, 48);
		frmAchmeRegister.getContentPane().add(lblAtm);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBackground(new Color(144, 238, 144));
		btnLogin.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IServerConnection connect = ServerConnectionSingleton.getInstance();
				System.out.println(connect.getId());
				frmAchmeRegister.dispose();
				AcmeStockMarketSystemUI frame1 = new AcmeStockMarketSystemUI();
				frame1.setVisible(true);
				
				
			}
		});
		btnLogin.setBounds(199, 156, 89, 23);
		frmAchmeRegister.getContentPane().add(btnLogin);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Consolas", Font.PLAIN, 11));
		lblName.setBounds(80, 76, 46, 14);
		frmAchmeRegister.getContentPane().add(lblName);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Consolas", Font.PLAIN, 11));
		lblPassword.setBounds(80, 115, 66, 14);
		frmAchmeRegister.getContentPane().add(lblPassword);
		
		txtName = new JTextField();
		txtName.setBounds(156, 73, 185, 20);
		frmAchmeRegister.getContentPane().add(txtName);
		txtName.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setBounds(156, 112, 185, 20);
		frmAchmeRegister.getContentPane().add(txtPassword);
		txtPassword.setColumns(10);
	}
}
