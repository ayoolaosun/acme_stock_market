/**
 * 
 */
package stockMarketTesting;

import java.util.ArrayList;

/**
 * @author Ayo
 * Manages Selling to Stock Market
 * Implements: IServerTransaction
 * @param Stock
 *  
 */
public class SellStockToServer implements IServerTransaction {
	
	private Stock sellStock;
	IServerConnection connect = ServerConnectionSingleton.getInstance();
	private String id;
	private String transaction; 
	private CommunicationProtocol communicationProtocol;
	private String communication;
    ArrayList<String> returnedMessage = new ArrayList<String>();

	
	public SellStockToServer(Stock sellStock, String id){
		this.sellStock = sellStock;
		this.setId(id);
		this.setTransaction("SELL");
		
	}
	/**
	 * Sets the structure of protocol to be sent to Server.
	 * @param sellStock
	 */
	public ArrayList<String> run(){
		this.communicationProtocol = new CommunicationProtocol(this.getTransaction(),this.sellStock.getName(),this.sellStock.getSellQuantity(),this.getId());
		this.communicationProtocol.getCommunicationProtocol();
		this.communication = this.communicationProtocol.getCommunicationProtocol();
		returnedMessage =connect.readMessage(this.communication);
		return returnedMessage;
	}
	/**
	 * @return the transaction
	 */
	public String getTransaction() {
		return transaction;
	}
	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	public String test(){
		return this.communication;
	}
}
