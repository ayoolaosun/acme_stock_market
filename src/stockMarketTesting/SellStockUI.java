package stockMarketTesting;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.SwingConstants;

public class SellStockUI extends JFrame {

	private JPanel contentPane;
	private JFrame frame;
	private JTextField txtStockName;
	private JTextField txtStockQuantity;
	Collection<Stock> sellStocks=  new ArrayList<Stock>();
	private IStockAccount stockAccount = StockAccount.getStockAccount();
	private ServerCommunicationFacade communicationFacade = new ServerCommunicationFacade();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SellStockUI frame = new SellStockUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SellStockUI() {
		setTitle("Sell Stock");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 925, 468);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("BACK");
		button.setBackground(new Color(240, 240, 240));
		button.setFont(new Font("Consolas", Font.PLAIN, 11));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AcmeStockMarketSystemUI frame1 = new AcmeStockMarketSystemUI();
				frame1.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
		button.setBounds(10, 19, 89, 23);
		contentPane.add(button);
		
		JLabel lblAcmeStockMarket = new JLabel("Acme Stock Market");
		lblAcmeStockMarket.setFont(new Font("Consolas", Font.BOLD, 24));
		lblAcmeStockMarket.setBounds(164, 0, 246, 48);
		contentPane.add(lblAcmeStockMarket);
		
		JLabel label_1 = new JLabel("Stock Name");
		label_1.setFont(new Font("Consolas", Font.PLAIN, 11));
		label_1.setBounds(0, 64, 75, 14);
		contentPane.add(label_1);
		
		txtStockName = new JTextField();
		txtStockName.setFont(new Font("Consolas", Font.PLAIN, 11));
		txtStockName.setColumns(10);
		txtStockName.setBounds(150, 61, 172, 20);
		contentPane.add(txtStockName);
		
		txtStockQuantity = new JTextField("");
		txtStockQuantity.setFont(new Font("Consolas", Font.PLAIN, 11));
		txtStockQuantity.setColumns(10);
		txtStockQuantity.setBounds(150, 116, 45, 20);
		contentPane.add(txtStockQuantity);
		
		JLabel lblSellList = new JLabel("");
		lblSellList.setFont(new Font("Consolas", Font.PLAIN, 11));
		lblSellList.setVerticalAlignment(SwingConstants.TOP);
		lblSellList.setHorizontalAlignment(SwingConstants.CENTER);
		lblSellList.setBounds(510, 35, 177, 209);
		contentPane.add(lblSellList);

		JLabel lblStockList = new JLabel("");
		lblStockList.setFont(new Font("Consolas", Font.PLAIN, 11));
		lblStockList.setHorizontalAlignment(SwingConstants.CENTER);
		lblStockList.setVerticalAlignment(SwingConstants.TOP);
		lblStockList.setBounds(726, 28, 173, 209);
		contentPane.add(lblStockList );
		
		JButton btnAddToSell = new JButton("ADD TO SELL STOCK ORDER");
		btnAddToSell.setBackground(new Color(240, 240, 240));
		btnAddToSell.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnAddToSell.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Check if Stock exists in Account Select Stocks 
				String stockName;
				int quantity;
				stockName = txtStockName.getText();
				quantity= Integer.parseInt(txtStockQuantity.getText());
				communicationFacade.checkStock(stockName, quantity);
				sellStocks = stockAccount.getSellStocks();
				String appendString ="<html><U>Stock Name   Quantity</U>";
				for (Stock stock: sellStocks) {
					appendString = appendString + "<p>" + stock.getName()+ "= " + Integer.toString(stock.getSellQuantity())+ "</p> ";
				}
				lblSellList.setText(appendString + "</html>");
			}
		});
		btnAddToSell.setBounds(220, 115, 232, 23);
		contentPane.add(btnAddToSell);
		
		JButton btnPlaceSellStock = new JButton("PLACE SELL STOCK ORDER");
		btnPlaceSellStock.setBackground(new Color(240, 240, 240));
		btnPlaceSellStock.setFont(new Font("Consolas", Font.PLAIN, 11));
		btnPlaceSellStock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				communicationFacade.sellStock(sellStocks);
				Collection<Stock> stocks = stockAccount.getStocks().values();
				String appendTest ="<html><U>Stock Name   Quantity</U>";
				for (Stock stock: stocks) {
					appendTest = appendTest +"<p>"+ stock.getName()+ " " + Integer.toString(stock.getQuantity())+ "</p> ";
				}
				lblStockList.setText(appendTest +"</html>");
				stockAccount.getSellStocks().clear();
				lblSellList.setText("");
			}
		});
		btnPlaceSellStock.setBounds(150, 193, 203, 23);
		contentPane.add(btnPlaceSellStock);
		
		JButton button_3 = new JButton("CANCEL");
		button_3.setBackground(new Color(240, 240, 240));
		button_3.setFont(new Font("Consolas", Font.PLAIN, 11));
		button_3.setBounds(363, 193, 89, 23);
		contentPane.add(button_3);
		
		JLabel label_2 = new JLabel("Stock Quantity");
		label_2.setFont(new Font("Consolas", Font.PLAIN, 11));
		label_2.setBounds(0, 119, 89, 14);
		contentPane.add(label_2);
		Collection<Stock> stocks = stockAccount.getStocks().values();
		String appendString ="<html><U>Stock Name   Quantity</U>";
		for (Stock stock: stocks) {
			appendString = appendString + "<p>" + stock.getName()+ "= " + Integer.toString(stock.getQuantity())+ "</p> ";
		}
		lblStockList.setText(appendString + "</html>");
		
		
	}
}
