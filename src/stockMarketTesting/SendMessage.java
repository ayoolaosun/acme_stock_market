/**
 * 
 */
package stockMarketTesting;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Ayo
 * Sends Message to Server
 * Implements: IServerCommunication
 */
public class SendMessage implements IServerCommunication {
	IServerConnection connect  = ServerConnectionSingleton.getInstance();
	private ServerBroker broker;

	/**
	 * Initial Connection with Server to attain private Key
	 */
	public void initialMessage(){
		//Returns the singleton server connection Object 
		connect.connectToServer();
	}
	
	/**
	 * Construct Buying of Stocks from Server
	 * @param Collection 
	 * 	@returns Message from server
	 */
	public ArrayList<String> buyStockFromServer(Collection<Stock> stocks)
	{
		
	    ArrayList<String> returnedMessage = new ArrayList<String>();
		this.broker = new ServerBroker();
		for(Stock stock : stocks){
			BuyStockFromServer buyStock= new BuyStockFromServer(stock, this.connect.getId());
			this.broker.takeOrder(buyStock);  	    
		}
		returnedMessage = this.broker.placeOrder();
		return returnedMessage;
		
	}
	
	/**
	 * Construct Selling of Stocks to the Stock Market
	 * @param stocks
	 * @returns Message from server
	 */
	public ArrayList<String> sellStockToServer(Collection<Stock> stocks)
	{
	    ArrayList<String> returnedMessage = new ArrayList<String>();
		this.broker = new ServerBroker();
		for(Stock stock : stocks){
			SellStockToServer sellStock= new SellStockToServer(stock, this.connect.getId());
			this.broker.takeOrder(sellStock);  	    
		}
		returnedMessage = this.broker.placeOrder();
		return returnedMessage;
	}
	
	
	
}
