/**
 * 
 */
package stockMarketTesting;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Ayo
 * Manages Taking and Placing of Orders (Buy or Sell)
 */
public class ServerBroker {
	
	private List<IServerTransaction> transactionList = new ArrayList<IServerTransaction>(); 
	
	/**
	 * Takes Order BuyStockFromServer or SellStockToServer to Server
	 * @param IServerTransaction
	 */
	public void takeOrder(IServerTransaction transaction)
	{
		transactionList.add(transaction);
	}
	
	/**
	 * 
	 * @returns Purchase Message from Server.
	 */
	public ArrayList<String> placeOrder()
	{
		//Place all orders to server Buy or Sell
		//Adds the new message from server onto a List of Array
	    ArrayList<String> returnedMessage = new ArrayList<String>();
	      for (IServerTransaction transaction : transactionList) {
	    	  ArrayList<String> appendReturnedMessage = new ArrayList<String>();
	    	  transaction.run();
	    	  returnedMessage.addAll(appendReturnedMessage);
	      }
	      transactionList.clear();
	      return returnedMessage;
	}
}
