/**
 * 
 */
package stockMarketTesting;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
/**
 * @author Ayo
 *
 */
public class ServerCommunicationFacade {
	OperationSelectionFactory operationSelection = new OperationSelectionFactory();
	IStockAccount stockAccount = StockAccount.getStockAccount();
	IServerConnection connect = ServerConnectionSingleton.getInstance();

	
	/**
	 * Uses the operation class to create 
	 * sendMessage class, uses the 
	 * buyStockFrom Server to
	 * continue buy stock
	 * transaction
	 * @param stocks
	 * 
	 */
	public ArrayList<String> buyStock(Collection<Stock> stocks){
		//Send Message
		IServerCommunication newSendMessage;
		newSendMessage = this.operationSelection.selectOperation("sendMessage");
	    ArrayList<String> returnedMessage = new ArrayList<String>();
	    returnedMessage = ((SendMessage) newSendMessage).buyStockFromServer(stocks);
		stockAccount.getBuyStocks().clear();	
		return returnedMessage;	
	}
		
	
	
	/**
	 * Uses the operation class to create 
	 * sendMessage class, uses the 
	 * sellStockFrom Server to
	 * continue sell stock
	 * transaction
	 * @param stocks
	 */
	public  ArrayList<String> sellStock(Collection<Stock> stocks){
		//this.setStocks(stocks);
		//Send Messsage
		IServerCommunication newSendMessage;
		System.out.println(newSendMessage = this.operationSelection.selectOperation("sendMessage"));
		ArrayList<String> returnedMessage = new ArrayList<String>();
		returnedMessage = ((SendMessage) newSendMessage).sellStockToServer(stocks);
		stockAccount.getSellStocks().clear();
		return returnedMessage;
		
	}
	
	/**
	 * Allows viewing of stock Trend
	 * @return String
	 */
	public  ArrayList<String> displayTrends(){
	    ArrayList<String> returnedMessage = new ArrayList<String>();
		String displayStockTrends = "DISP:";
		displayStockTrends  = displayStockTrends + connect.getId();
		returnedMessage=connect.readMessage(displayStockTrends);
		return returnedMessage;
		
	}
	
	/**
	 * Allows viewing of last five trend of all stocks
	 * @return String
	 */
	public String disLastFiveTrends(){
		return null;
		
	}

	/**
	 * Creating of stock to buy and adding it to the buyList of accounts
	 * @param name
	 * @param buyQuantity
	 */
	public void createStock(String name, int buyQuantity){
		//For each stock in stocks
		//If stock name does not exists create new stock
		//else change stock buy quantity append buy quantity
		
		name = name.toUpperCase();
		if (!(stockAccount.getStocks().isEmpty())){
			//Check if stock exists using hash map
			//if true add stock to buy list
			//else add stock to 
			//create stock
			if(stockAccount.getStocks().containsKey(name)){
				Stock buyStock = stockAccount.getStocks().get(name);
				if (stockAccount.getBuyStocks().contains(buyStock)){
					//Update stock hash table content has well 
					stockAccount.getStocks().get(name).setBuyQuantity(buyQuantity + stockAccount.getStocks().get(name).getBuyQuantity());
				}else{
					//Add to buy stock if stock does not exist in buyStock List, 
					//but exists in Hash Table
					stockAccount.getStocks().get(name).setBuyQuantity(buyQuantity);
					stockAccount.addToBuyStock(stockAccount.getStocks().get(name));
				}
			}else{
				//create new stock 
				name = name.toUpperCase();
				Stock newStock = new Stock(name);
				newStock.setBuyQuantity(buyQuantity);
				stockAccount.addToBuyStock(newStock);
				stockAccount.addToStock(newStock);
				//add to hash map
				
				//add to buyList
			}

		}else{
			//Current Stock List is empty, add stock to list.
			name = name.toUpperCase();
			Stock newStock = new Stock(name);
			name =name.toUpperCase();
			newStock.setBuyQuantity(buyQuantity);
			stockAccount.addToBuyStock(newStock);
			stockAccount.addToStock(newStock);
		}
		
	}
	
	/**
	 * 
	 * @param name
	 * @param buyQuantity
	 */
	public void checkStock(String name, int sellQuantity){
		//Select From Current Stock List
		//If name exists add to sell List
		//Add stock to Sell Stock List
		name = name.toUpperCase();
		if(stockAccount.getStocks().containsKey(name)){
			Stock sellStock = stockAccount.getStocks().get(name);
			if (stockAccount.getSellStocks().contains(sellStock )){ 
				//Update stock hash table content has well 
				stockAccount.getStocks().get(name).setSellQuantity(sellQuantity + stockAccount.getStocks().get(name).getSellQuantity());
			}else{
				//Ensure there is enough stock Left before selling stock 
				if(sellStock.getQuantity() >= sellQuantity){
					stockAccount.getStocks().get(name).setSellQuantity(sellQuantity);
					stockAccount.addToSellStocks(stockAccount.getStocks().get(name));
				}
			}
		}
		
	}
	

}
