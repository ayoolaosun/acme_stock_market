package stockMarketTesting;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.net.Socket;
import java.util.ArrayList;



/**
 * Singleton Connection to the Stock Market Server
 * Implements the ServerConnectionSingletonInterface
 */
public class ServerConnectionSingleton implements IServerConnection {
	
	private static ServerConnectionSingleton connection = new ServerConnectionSingleton();
    private String id;
    Socket clientSocket;
    String serverMessage;
    
	/**	
	 * Stops ServerConnectionSingleton From 
	 * Being Instantiated More Than Once
	 */
	private ServerConnectionSingleton(){
           this.connectToServer();
	}
	
	/**
	 * @return ServerConnection
	 */
	public static ServerConnectionSingleton getInstance(){
		return connection;
	}
	
	/**
	 * Returns the Instantiated Socket to Client  
	 */
	public void connectToServer(){
		try {
            clientSocket = new Socket("localhost", 5000);
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            //creates a new print writer from outputstream
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
            out.println("REGI");
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            this.id = in.readLine();
			final String[] allId = this.id.split(":");
			this.id = allId[allId.length-1];
		} catch (IOException e){
			System.out.println("An Error Has Occured - " + e);
        }

	}
	/**
	 * Sends message to server
	 * @param message
	 */
	public ArrayList<String> readMessage(String message){
//		try {
			sendMessage(message);
			ClientRead myRead;
		    myRead = new ClientRead(clientSocket);
		    StartThreads(myRead);
		    ArrayList<String> returnedMessage = new ArrayList<String>();
		    returnedMessage = myRead.getReturnedMessage();
		    return returnedMessage;
			
	}
	
	/**
	 * Reads message from server
	 * @param message
	 */
	public void sendMessage(String message){
		try {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            //creates a new print writer from outputstream
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
            out.println(message);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

		} catch (IOException e){
			System.out.println("An Error Has Occured - " + e);
        }
			
	}
	
	
	public void StartThreads(ClientRead myRead)
    {	
        Thread t1 = new Thread(myRead);
        t1.start();
     
    }
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	private void setId(String id) {
		this.id = id;
	}
	
	
	
	public String getServerMessage(){
		return this.serverMessage;
	}
	
	public void closeConnection(){
		try {
			clientSocket.close();
			System.out.println("Connection Closed");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


