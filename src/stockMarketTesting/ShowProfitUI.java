package stockMarketTesting;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.SwingConstants;

public class ShowProfitUI extends JFrame {

	private JPanel contentPane;
	private IStockAccount stockAccount = StockAccount.getStockAccount();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowProfitUI frame = new ShowProfitUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShowProfitUI() {
		setTitle("Profit");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("BACK");
		button.setFont(new Font("Consolas", Font.PLAIN, 11));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AcmeStockMarketSystemUI frame1 = new AcmeStockMarketSystemUI();
				frame1.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
		button.setBounds(10, 19, 89, 23);
		contentPane.add(button);
		
		JLabel label = new JLabel("Achme Stock Market");
		label.setFont(new Font("Consolas", Font.BOLD, 24));
		label.setBounds(164, 0, 246, 48);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Balance");
		label_1.setForeground(SystemColor.controlDkShadow);
		label_1.setFont(new Font("Consolas", Font.PLAIN, 12));
		label_1.setBounds(105, 75, 63, 23);
		contentPane.add(label_1);
		
		JLabel currentBalance = new JLabel("");
		currentBalance.setForeground(Color.GRAY);
		currentBalance.setFont(new Font("Consolas", Font.BOLD, 13));
		currentBalance.setBounds(105, 109, 107, 48);
		contentPane.add(currentBalance);
		currentBalance.setText("�" + Double.toString(stockAccount.getCurrentBalance()));
		
		
		JLabel label_3 = new JLabel("Profit");
		label_3.setVerticalAlignment(SwingConstants.TOP);
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setForeground(Color.GRAY);
		label_3.setFont(new Font("Consolas", Font.PLAIN, 11));
		label_3.setBounds(273, 79, 63, 23);
		contentPane.add(label_3);
		
		JLabel currentProfit = new JLabel("");
		currentProfit.setForeground(Color.GRAY);
		currentProfit.setFont(new Font("Consolas", Font.BOLD, 13));
		currentProfit.setBounds(283, 109, 107, 48);
		Double profitMade =  stockAccount.getCurrentBalance()- 10000 ;
		Double.toString(profitMade);
		currentProfit.setText("�" + profitMade);
		contentPane.add(currentProfit);
	}
}
