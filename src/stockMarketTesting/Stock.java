/**
 * 
 */
package stockMarketTesting;

/**
 * @author Ayo
 *
 */
public class Stock {
	private String name;
	private int quantity = 0;
	private int buyQuantity= 0;
	private int sellQuantity= 0;
	private String trendId= "";
	
	public Stock(String name){
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() 
	{
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	private void setName(String name) 
	{
		this.name = name;
	}

	/**
	 * @return the trendId
	 */
	public String getTrendId() {
		return trendId;
	}

	/**
	 * @param trendId the trendId to set
	 */
	public void setTrendId(String trendId) {
		this.trendId = trendId;
	}

	/**
	 * @return the buyQuantity
	 */
	public int getBuyQuantity() {
		return buyQuantity;
	}

	/**
	 * @param buyQuantity the buyQuantity to set
	 */
	public void setBuyQuantity(int buyQuantity) {
		this.buyQuantity = buyQuantity;
	}

	/**
	 * @return the sellQuantity
	 */
	public int getSellQuantity() {
		return sellQuantity;
	}

	/**
	 * @param sellQuantity the sellQuantity to set
	 */
	public void setSellQuantity(int sellQuantity) {
		this.sellQuantity = sellQuantity;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	
	
}
