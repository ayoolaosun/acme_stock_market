package stockMarketTesting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

public class StockAccount implements IStockAccount{
	private double currentBalance = 10000.00;
	private static StockAccount newStockAccount = new StockAccount();
	//private Collection<Stock> stocks =  new ArrayList<Stock>(); 
	private Collection<Stock> buyStocks=  new ArrayList<Stock>(); 
	private Collection<Stock> sellStocks=  new ArrayList<Stock>(); 
	Hashtable<String, Stock> stocks = new Hashtable<String, Stock>(); 
	
	
	private StockAccount(){
		
	}
	
	public static StockAccount getStockAccount(){
		return  newStockAccount;
	}

	/**
	 * @return the stocks
	 */
	public Hashtable<String, Stock> getStocks() {
		return stocks;
	}

	
	
	/**
	 * Append stock to Stock List
	 * @param stock
	 */
	public void addToStock(Stock stock){
		stocks.put(stock.getName(), stock);
	}
	
	/**
	 * Removes the stock from List
	 * @param stock
	 */
	public void removeFromStock(Stock stock){
		this.stocks.remove(stock.getName());
	}

	/**
	 * @return the sellStocks
	 */
	public Collection<Stock> getSellStocks() {
		return sellStocks;
	}

	/**
	 * @param sellStocks the sellStocks to set
	 */
	public void setSellStocks(Collection<Stock> sellStocks) {
		this.sellStocks = sellStocks;
	}

	/**
	 * Add to the Sell Stock List
	 * @param newStock
	 */
	public void addToSellStocks(Stock newStock){
		this.sellStocks.add(newStock);
	}
	
	/**
	 * Removes the sell stock from List
	 * @param newStock
	 */
	public void removeFromSellStock(Stock newStock){
		this.sellStocks.remove(newStock);
	}
	
	/**
	 * @return the buyStocks
	 */
	public Collection<Stock> getBuyStocks() {
		return buyStocks;
	}

	/**
	 * @param buyStocks the buyStocks to set
	 */
	public void setBuyStocks(Collection<Stock> buyStocks) {
		this.buyStocks = buyStocks;
	}
	
	/**
	 * Append to Buy Stock Collection
	 * @param newStock
	 */
	public void addToBuyStock(Stock newStock){
			this.buyStocks.add(newStock);
	}
	/**
	 * Remove from the buy Stock Collection
	 * @param newStock
	 */
	public void removeFromBuyStock(Stock newStock){
		this.buyStocks.remove(newStock);
	}

	
	
	/**
	 * Subtract from balance
	 * @param subtractAmount
	 */
	public void subtractFromBalance(double subtractAmount){
		this.setCurrentBalance(this.getCurrentBalance()- subtractAmount);
	}
	
	/**
	 * Add to Balance
	 * @param addedAmount
	 */
	public void addToBalance(double addedAmount){
		this.setCurrentBalance(this.getCurrentBalance() + addedAmount);
	}
	
	

	/**
	 * @return the currentBalance
	 */
	public double getCurrentBalance() {
		return currentBalance;
	}

	/**
	 * @param currentBalance the currentBalance to set
	 */
	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	
	
}
