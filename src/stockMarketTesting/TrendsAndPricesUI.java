package stockMarketTesting;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class TrendsAndPricesUI extends JFrame {

	private JPanel contentPane;
	IServerConnection connect = ServerConnectionSingleton.getInstance();
	private ServerCommunicationFacade communicationFacade = new ServerCommunicationFacade();
    ArrayList<String> returnedMessage = new ArrayList<String>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TrendsAndPricesUI frame = new TrendsAndPricesUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TrendsAndPricesUI() {
		setTitle("Stock Trend List");
		returnedMessage = communicationFacade.displayTrends();
		try {		 
		    TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
		    //Handle exception
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 481, 609);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblStockList = new JLabel("New label");
		lblStockList.setFont(new Font("Consolas", Font.PLAIN, 10));
		lblStockList.setBounds(10, 53, 445, 506);
		contentPane.add(lblStockList);
		
		String appendString ="<html>Stock Data<table><hr></br>";
		
		for(String aMessage: returnedMessage){
			appendString = appendString + "<tr><p>" + aMessage+ "</p></tr><hr>";
		}
		
		lblStockList.setText(appendString + "</table></html>");
		
		
		
		
		JButton button = new JButton("BACK");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AcmeStockMarketSystemUI frame1 = new AcmeStockMarketSystemUI();
				frame1.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
		button.setBounds(10, 19, 89, 23);
		contentPane.add(button);
		
		JLabel lblAcmeStockMarket = new JLabel("Acme Stock Market");
		lblAcmeStockMarket.setFont(new Font("Consolas", Font.BOLD, 24));
		lblAcmeStockMarket.setBounds(164, 0, 246, 48);
		contentPane.add(lblAcmeStockMarket);
		
	
		
	}
}
