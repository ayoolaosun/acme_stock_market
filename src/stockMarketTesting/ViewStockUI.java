package stockMarketTesting;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Color;

public class ViewStockUI extends JFrame {

	private JPanel contentPane;
	
	private IStockAccount stockAccount = StockAccount.getStockAccount();
	Collection<Stock> stocks = stockAccount.getStocks().values();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewStockUI frame = new ViewStockUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewStockUI() {
		setTitle("View Stocks");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 506);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("BACK");
		button.setForeground(new Color(128, 128, 128));
		button.setFont(new Font("Consolas", Font.PLAIN, 12));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AcmeStockMarketSystemUI frame1 = new AcmeStockMarketSystemUI();
				frame1.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
		button.setBounds(10, 19, 89, 23);
		contentPane.add(button);
		
		JLabel txtCurrentStockList = new JLabel("");
		txtCurrentStockList.setForeground(new Color(105, 105, 105));
		txtCurrentStockList.setFont(new Font("Consolas", Font.PLAIN, 11));
		txtCurrentStockList.setVerticalAlignment(SwingConstants.TOP);
		txtCurrentStockList.setBounds(20, 59, 287, 421);
		contentPane.add(txtCurrentStockList);
		String test2 ="<html><U>Stock Name   Quantity</U>";
		for (Stock stock: stocks) {
	    	  test2 = test2 + "<p>" + stock.getName()+ "= " + Integer.toString(stock.getQuantity())+ "</p> ";
	     }
		txtCurrentStockList.setText(test2 + "</html>");
		
		JLabel lblBalance = new JLabel("Balance");
		lblBalance.setForeground(new Color(105, 105, 105));
		lblBalance.setFont(new Font("Consolas", Font.PLAIN, 12));
		lblBalance.setBounds(297, 59, 63, 23);
		contentPane.add(lblBalance);
		
		JLabel lblCurrentBalance = new JLabel("");
		lblCurrentBalance.setForeground(new Color(128, 128, 128));
		lblCurrentBalance.setFont(new Font("Consolas", Font.BOLD, 13));
		lblCurrentBalance.setBounds(297, 74, 107, 48);
		contentPane.add(lblCurrentBalance);
		lblCurrentBalance.setText("�" + Double.toString(stockAccount.getCurrentBalance()));
		
		JLabel label = new JLabel("Acme Stock Market");
		label.setFont(new Font("Consolas", Font.BOLD, 24));
		label.setBounds(114, 3, 246, 48);
		contentPane.add(label);
		
		JLabel lblProfit = new JLabel("Profit");
		lblProfit.setForeground(new Color(128, 128, 128));
		lblProfit.setFont(new Font("Consolas", Font.PLAIN, 11));
		lblProfit.setHorizontalAlignment(SwingConstants.CENTER);
		lblProfit.setVerticalAlignment(SwingConstants.TOP);
		lblProfit.setBounds(282, 134, 63, 23);
		contentPane.add(lblProfit);
		
		JLabel lblCurrentProfitMade = new JLabel("");
		lblCurrentProfitMade.setForeground(Color.GRAY);
		lblCurrentProfitMade.setFont(new Font("Consolas", Font.BOLD, 13));
		lblCurrentProfitMade.setBounds(297, 148, 107, 48);
		contentPane.add(lblCurrentProfitMade);
		Double profitMade =  stockAccount.getCurrentBalance()- 10000 ;
		Double.toString(profitMade);
		lblCurrentProfitMade.setText("�" + profitMade);

	}
}
