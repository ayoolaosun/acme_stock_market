package testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import stockMarketTesting.BuyStockFromServer;
import stockMarketTesting.IServerConnection;
import stockMarketTesting.ServerConnectionSingleton;
import stockMarketTesting.Stock;

public class BuyStockFromServerTest {
	IServerConnection connection = ServerConnectionSingleton.getInstance();
	private Stock stock = new Stock("MICROSOFT");
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {	
		this.stock.setBuyQuantity(3);
		BuyStockFromServer test = new BuyStockFromServer(stock, connection.getId());
		test.run();
		assertEquals("BUY:MICROSOFT:3:test", test.test());
	}

}
