package testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import stockMarketTesting.CommunicationProtocol;
import stockMarketTesting.IServerConnection;
import stockMarketTesting.ServerConnectionSingleton;

public class CommunicationProtocolTest {
	IServerConnection connection = ServerConnectionSingleton.getInstance();
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		
		CommunicationProtocol test = new CommunicationProtocol("BUY", "MICROSOFT", 3, connection.getId()); 
		System.out.println(test.getCommunicationProtocol());
		assertEquals("BUY:MICROSOFT:3:test", test.getCommunicationProtocol());
	}

}
