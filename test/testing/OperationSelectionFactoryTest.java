package testing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import stockMarketTesting.IServerCommunication;
import stockMarketTesting.OperationSelectionFactory;
import stockMarketTesting.ReceiveMessage;
import stockMarketTesting.SendMessage;

public class OperationSelectionFactoryTest {
	private IServerCommunication communication;
	private OperationSelectionFactory operation = new OperationSelectionFactory();
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		communication = this.operation.selectOperation("receiveMessage"); 
		assertTrue(communication instanceof  ReceiveMessage); 
		communication = this.operation.selectOperation("sendMessage"); 
		assertTrue(communication instanceof  SendMessage); 
		
	}

}
