package testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import stockMarketTesting.IServerConnection;
import stockMarketTesting.SellStockToServer;
import stockMarketTesting.ServerConnectionSingleton;
import stockMarketTesting.Stock;

public class SellStockToServerTest {
	IServerConnection connection = ServerConnectionSingleton.getInstance();
	private Stock stock = new Stock("MICROSOFT");
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		this.stock.setSellQuantity(3);
		SellStockToServer test = new SellStockToServer(stock, connection.getId());
		test.run();
		assertEquals("SELL:MICROSOFT:3:test", test.test());
	}

}
