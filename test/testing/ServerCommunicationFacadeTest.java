/**
 * 
 */
package testing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import stockMarketTesting.ServerCommunicationFacade;
import stockMarketTesting.Stock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
/**
 * @author Ayo
 *
 */
public class ServerCommunicationFacadeTest {

	Collection<Stock> stocks =  new ArrayList<Stock>();
	Stock newStock1 = new Stock("MICROSOFT");
	Stock newStock2 = new Stock("GOOGLE");
	Stock newStock3 = new Stock("APPLE");
	
	
	
	@Before
	public void setUp() throws Exception {
		this.stocks.add(this.newStock1);
		this.stocks.add(this.newStock2);
		this.stocks.add(this.newStock3);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link stockMarketTesting.ServerCommunicationFacade#buyStock(stockMarketTesting.Stock[])}.
	 */
	@Test
	public void testBuyStock() {
		
		ServerCommunicationFacade test = new ServerCommunicationFacade();
		test.buyStock(this.stocks);
		//assertEquals(this.stocks, test.getStocks());

		
	}

	/**
	 * Test method for {@link stockMarketTesting.ServerCommunicationFacade#sellStock(stockMarketTesting.Stock[])}.
	 */
	@Ignore @Test 
	public void testSellStock() {
		fail("Not yet implemented"); // TODO
	}

}
